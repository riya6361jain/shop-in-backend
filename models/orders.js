const mongoose = require('mongoose')

const orderSchema = mongoose.Schema({
    basket:{type:Array},
    amount:{type:Number},
    createdAt:{type:String}
}) 

const Order = mongoose.model('orderData',orderSchema);
module.exports = Order;